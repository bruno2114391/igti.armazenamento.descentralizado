﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(IGTI.PA.ArmazenamentoDescentralizado.Startup))]

namespace IGTI.PA.ArmazenamentoDescentralizado
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

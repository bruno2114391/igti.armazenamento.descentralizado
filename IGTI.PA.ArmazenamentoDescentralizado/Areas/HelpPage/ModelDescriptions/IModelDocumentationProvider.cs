using System;
using System.Reflection;

namespace IGTI.PA.ArmazenamentoDescentralizado.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}
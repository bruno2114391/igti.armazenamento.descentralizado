﻿using System.Web;
using System.Web.Mvc;

namespace IGTI.PA.ArmazenamentoDescentralizado
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

﻿using System.Web;
using System.Web.Optimization;

namespace IGTI.PA.ArmazenamentoDescentralizado
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                 "~/Scripts/plugins/jquery-ui/jquery-ui-1.11.4.js",
                 "~/Scripts/plugins/jquery-ui/jquery.ui.widget.js",
                 "~/Scripts/jquery.fileupload.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // Angular css
            bundles.Add(new StyleBundle("~/Content/angular").Include(
                        "~/Content/angular-toastr.css",
                        "~/Content/xeditable.css",
                        "~/Content/tree-control.css",
                        "~/Content/tree-control-attribute.css",
                        "~/Content/plugins/ngtags/ng-tags-input.css"));


            // Angular
            bundles.Add(new ScriptBundle("~/App/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/promise-tracker.js",

                        "~/Scripts/angular-sanitize.js",
                        "~/Scripts/angular-touch.js",
                        "~/Scripts/angular-toastr.tpls.js",
                        "~/Scripts/ngStorage.js",
                        "~/Scripts/underscore.js",
                        "~/Scripts/plugins/moment/moment.js",
                        "~/Scripts/angular-moment.js",
                        "~/Scripts/angular-messages.js",
                        "~/Scripts/plugins/waitingfor.js",
                        "~/Scripts/angular-cookies.min.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-tree-control.js",
                        "~/Scripts/checklist-model.js",
                        "~/Scripts/mg/packages/angular-ui/mask.min.js",
                        "~/Scripts/angular-filter.js",
                        "~/Scripts/plugins/ngtags/ng-tags-input.js",
                        "~/Scripts/ui-bootstrap-tpls.js"

                        ));

            // App
            bundles.Add(new ScriptBundle("~/Scripts/appangular").Include(
                        "~/Scripts/app/app.js",
                        "~/Scripts/app/config.js",
                        "~/scripts/app/services/service.js",
                        "~/scripts/app/upload.js"
                      ));


            bundles.Add(new ScriptBundle("~/Scripts/authenticationService").Include(
                      "~/Scripts/app/authentication/authenticationController.js"));
        }
    }
}

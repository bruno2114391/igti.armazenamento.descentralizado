﻿(function () {
    'use strict';

    var app = angular.module('app');

    app.config(['toastrConfig', function (toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });

    }]);


    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.withCredentials = true;


    }]);

    app.factory('httpRequestInterceptor', ['$q', 'toastr', '$cookies', '$localStorage',
        function ($q, toastr, $cookies, $localStorage) {
            return {
                request: function ($config) {
                    if ($localStorage["access_token"] != "")
                        $config.headers['Authorization'] = 'Bearer ' + $localStorage["access_token"];


                    return $config;

                },
                // On response failure
                responseError: function (rejection) {

                    var message = "Authorization has been denied for this request.";

                    if (rejection.status == 401) {

                        $localStorage["access_token"] = undefined;
                        window.location.href = window.urlBase;
                    }

                    if (rejection.data.error_description != undefined) {
                        toastr.error((rejection.data.error_description));
                    }
                    else if (rejection.data.modelState != undefined) {
                        angular.forEach(rejection.data.modelState, function (value, key) {
                            toastr.error(key.toUpperCase());
                        });
                    } else if (rejection.data.message == message || rejection.data.Message == message) {
                        toastr.error("MENSAGENS.SEMACESSOACAO");
                    }

                    if (common.cookieOAuthExpired()) {
                        common.logOff();
                    }


                    setTimeout(function () {
                        waitingDialog.hide();
                    }, 500);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                },


            };
        }]);

    app.directive('capitalize', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {
                var capitalize = function (inputValue) {
                    if (inputValue == undefined) inputValue = '';
                    var capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                }
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]); // capitalize initial value
            }
        };
    });

    app.directive('format', ['$filter', function ($filter) {
        'use strict';
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    return;
                }

                ctrl.$formatters.unshift(function () {
                    return $filter('number')(ctrl.$modelValue);
                });

                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[\,\.]/g, ''),
                        b = $filter('number')(plainNumber);

                    elem.val(b);

                    return plainNumber;
                });
            }
        };
    }]);



    app.directive('datepickerLocaldate', ['$parse', function ($parse) {
        var directive = {
            restrict: 'A',
            require: ['ngModel'],
            link: link
        };
        return directive;

        function link(scope, element, attr, ctrls) {
            var ngModelController = ctrls[0];

            // called with a JavaScript Date object when picked from the datepicker
            ngModelController.$parsers.push(function (viewValue) {
                console.log(viewValue); console.log(viewValue); console.log(viewValue);
                // undo the timezone adjustment we did during the formatting
                viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
                // we just want a local date in ISO format
                return viewValue;//.toISOString().substring(0, 10);
            });
        }
    }]);

    app.directive('datepickerEnd', ['$parse', function ($parse) {
        var directive = {
            restrict: 'A',
            require: ['ngModel'],
            link: link
        };
        return directive;

        function link(scope, element, attr, ctrls) {
            var ngModelController = ctrls[0];

            // called with a JavaScript Date object when picked from the datepicker
            ngModelController.$parsers.push(function (viewValue) {
                console.log(viewValue); console.log(viewValue); console.log(viewValue);
                // undo the timezone adjustment we did during the formatting
                viewValue.setHours(23);
                viewValue.setMinutes(59);
                // we just want a local date in ISO format
                return viewValue;//.toISOString().substring(0, 10);
            });
        }
    }]);
})();
﻿

(function () {

    'use strict';

    angular
        .module('app')
        .controller('cadastroDisciplinaController', ['$scope', '$http', 'toastr', '$localStorage', 'parseWithDate', 'serviceUpload'
            , function ($scope, $http, toastr, $localStorage, parseWithDate, serviceUpload) {

                var vm = this;
                vm.modelDiscplina = {
                    idBlockchain: ""
                };

                vm.usuarioLogado = $localStorage['usuarioNome'];
                vm.tipoUsuario = $localStorage['tipoUsuario'];

                vm.buscarAlunos = function () {
                    $http.get(window.urlApi + "Aluno/BuscarAlunos")
                        .then(function (response) {


                            vm.listaAlunos = response.data;
                            vm.modelDiscplina.usuarioId = vm.listaAlunos[0].id;

                        }, function errorCallback(error) {

                            toastr.error(error);

                        });
                };

                vm.buscarDisciplinas = function () {
                    $http.get(window.urlApi + "Aluno/BuscarDisciplinas")
                        .then(function (response) {


                            vm.listaDisciplinas = response.data;                            

                        }, function errorCallback(error) {

                            toastr.error(error);

                        });
                };

                vm.cadastrarNotaAluno = function () {

                    window.projetoAplicado.methods.registrarDisciplinaAluno(
                        [vm.modelDiscplina.idBlockchain, vm.modelDiscplina.disciplina, vm.modelDiscplina.nota])
                        .send({ from: '0xc9939790463aa0e4d0eeaa61958d99d5c9a57c15', gasPrice: '30000' },
                            function (error, result) {
                                if (error != null && error.code == 4001) {
                                    swal({
                                        title: 'Transação cancelada',
                                        text: result,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: 'OK',
                                        showConfirmButton: true
                                    }).then(
                                        function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                        }).catch(swal.noop);
                                } else {
                                    swal({
                                        title: 'Código transação',
                                        text: result,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: 'OK',
                                        closeOnCancel: false
                                    }).then(
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                vm.modelDiscplina.hash = result;
                                                vm.registrarDisciplinaNota();
                                            }
                                        }).catch(swal.noop);
                                }

                                console.log(result);
                            }).on('confirmation', function (confirmationNumber, receipt) {
                                if (confirmationNumber > 0 && confirmationNumber < 5)
                                    toastr.success(confirmationNumber + " transação confirmada!");

                            });
                };

                vm.registrarDisciplinaNota = function () {
                    $http.post(window.urlApi + "Aluno/CadastrarDisciplinaAluno", angular.toJson(vm.modelDiscplina))
                        .then(function (response) {
                            toastr.success('Disciplina cadastrado com sucesso!');
                            vm.buscarDisciplinas();

                        }, function errorCallback(error) {

                            toastr.error(error);

                        });
                };

                vm.buscarAlunos();
                vm.buscarDisciplinas();

            }]);
})();
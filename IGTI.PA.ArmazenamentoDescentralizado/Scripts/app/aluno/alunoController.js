﻿

(function () {

    'use strict';

    angular
        .module('app')
        .controller('alunoControllerController', ['$scope', '$http', 'toastr', '$localStorage', 'parseWithDate', 'serviceUpload'
            , function ($scope, $http, toastr, $localStorage, parseWithDate, serviceUpload) {


                var vm = this;
                $scope.fechamento = false;
                $scope.mensagem = "";

                var token = $localStorage["access_token"];

                vm.usuarioLogado = $localStorage['usuarioNome'];
                vm.tipoUsuario = $localStorage['tipoUsuario'];

                if (token == "" || token == undefined) {
                    $localStorage['access_token'] = undefined;
                    $localStorage['usuarioNome'] = undefined;
                    $localStorage['tipoUsuario'] = undefined;
                    location.href = window.urlBase + "/Home";
                }
                else if (vm.tipoUsuario == true)
                    location.href = window.urlBase + "/Professor";


                vm.sair = function () {
                    $localStorage['access_token'] = undefined;
                    location.href = window.urlBase + "/Home";

                };

                vm.modalLogin = function () {
                    $("#modalLoginForm").modal('show');
                };

                vm.modalAula = function (curso) {

                    $scope.srcVideo = curso.src;

                    var media = $("#video").get(0);
                    media.load();
                    $("#modalAula").modal('show');
                };

                vm.carregarCurso = function () {
                    $http.get(window.urlApi + "Curso/CarregarCurso",
                        {
                            transformResponse: function (json) {
                                return parseWithDate.parseWithDate((json), moment);
                            }
                        }).then(function (response) {

                            vm.listaCurso = response.data;

                            setTimeout(function () {


                            }, 500);


                        }, function errorCallback() {
                            toastr.error("Erro, tente novamente!");
                            setTimeout(function () {

                            }, 200);
                        });
                };

                vm.carregarCurso();

                $('#modalAula').on('hidden.bs.modal', function () {
                    var media = $("#video").get(0);
                    media.pause();
                    media.currentTime = 0;
                });

            }]);
})();
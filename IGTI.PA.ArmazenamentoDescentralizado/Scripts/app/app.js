﻿
(function () {
    'use strict';

    var app = angular
        .module('app', [
            // Angular modules                           
            'toastr',
            'ngStorage',
            'ngLocale',
            'ngTouch',
            'ui.bootstrap',
            'ajoslin.promise-tracker',
            'ngMessages',
            'ngCookies',
            'ngRoute',
            'treeControl',
            'checklist-model',
            'ui.mask'
        ]);


    app.factory('utilities', function () {
        return {
            formatDate: function (date) {

                if (date != null) {
                    var dateOut = new Date(date);
                    return dateOut;
                }
            },

            isNullOrEmpty: function (value) {
                return (!value || value == undefined || value == "" || value.length == 0 || value == null);
            },
            daysInMonth: function (month, year) {
                return new Date(year, month, 0).getDate();
            },
            isNumber: function (value) {
                return !isNaN(value)
            },
            isNullOrEmptyOrUndefined: function (value) {
                if (value === "" || value === null || typeof value === "undefined") {
                    return true;
                } else {
                    return false;
                }
            },
        };
    });

    app.filter('reverse', function () {
        return function (items) {
            if (items != undefined && items.length > 0)
                return items.slice().reverse();
        };
    });

    app.filter('changeproperties', ['$translate', function (translate) {

        return function (value) {
            return translate.instant(value.toUpperCase());
        };

    }]);

    //window.urlApi = "http://apicarbonelanches.biofocusambiental.com.br/api/";
    //window.urlBase = "http://carbonelanches.biofocusambiental.com.br";

    window.urlApi = "http://localhost/IGTI.PA.ArmazenamentoDescentralizado.Api/api/";
    window.urlBase = "http://localhost/IGTI.ArmazenamentoDescentralizado/";
    window.EXTENSIONSMOVIE = "avi, mpg4, mp4, mov   ";
}());


﻿(function () {

    'use strict';

    //https://ropsten.infura.io/v3/6450d08cdcad4f3aabc9492d04f87e8f
    //= new Web3(new Web3.providers.HttpProvider('https://api.infura.io/v1/jsonrpc/ropsten'));

    window.web3js;
    if (window.ethereum) {
        window.web3js = new Web3(window.ethereum);
        try {
            window.ethereum.enable().then(function () { });
        } catch (e) {
            aler("O usuário negou o acesso da conta ao DApp");
        }
    }
    //Navegadores DApp herdados
    else if (window.web3) {
        window.web3js = new Web3(web3js.currentProvider);
    }
    else {
        alert('Voçê precisa instalar o MetaMask!');
    }
    window.address = "0xf0d8a42c574cc810b0b372fce0415e21830ca795";
    window.abi = [
        {
            "constant": false,
            "inputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "RegistroAluno",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "Nome",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Nacionalidade",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Sexo",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Naturalidade",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Documento",
                            "type": "string"
                        }
                    ],
                    "internalType": "struct ProjetoAplicado.SAluno",
                    "name": "recebido_aluno",
                    "type": "tuple"
                }
            ],
            "name": "registrarAluno",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "RegistroAluno",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "Disciplina",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Nota",
                            "type": "string"
                        }
                    ],
                    "internalType": "struct ProjetoAplicado.SDisciplina",
                    "name": "recebido_disciplina",
                    "type": "tuple"
                }
            ],
            "name": "registrarDisciplinaAluno",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "registro_aluno",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "internalType": "uint32",
                    "name": "recebido_aluno",
                    "type": "uint32"
                }
            ],
            "name": "removerAluno",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "recebido_aluno",
                    "type": "uint256"
                },
                {
                    "internalType": "string",
                    "name": "recebido_disciplina",
                    "type": "string"
                }
            ],
            "name": "removerDisciplina",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "inputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "constructor"
        },
        {
            "payable": true,
            "stateMutability": "payable",
            "type": "fallback"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "internalType": "string",
                    "name": "s1",
                    "type": "string"
                },
                {
                    "internalType": "string",
                    "name": "s2",
                    "type": "string"
                }
            ],
            "name": "compareStringsbyBytes",
            "outputs": [
                {
                    "internalType": "bool",
                    "name": "",
                    "type": "bool"
                }
            ],
            "payable": false,
            "stateMutability": "pure",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "recebido_aluno",
                    "type": "uint256"
                }
            ],
            "name": "retoranarAluno",
            "outputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "RegistroAluno",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "Nome",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Nacionalidade",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Sexo",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Naturalidade",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Documento",
                            "type": "string"
                        }
                    ],
                    "internalType": "struct ProjetoAplicado.SAluno",
                    "name": "retorno_aluno",
                    "type": "tuple"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "internalType": "uint256",
                    "name": "recebido_aluno",
                    "type": "uint256"
                }
            ],
            "name": "retoranarDisciplinaMatriculadoAluno",
            "outputs": [
                {
                    "components": [
                        {
                            "internalType": "uint256",
                            "name": "RegistroAluno",
                            "type": "uint256"
                        },
                        {
                            "internalType": "string",
                            "name": "Disciplina",
                            "type": "string"
                        },
                        {
                            "internalType": "string",
                            "name": "Nota",
                            "type": "string"
                        }
                    ],
                    "internalType": "struct ProjetoAplicado.SDisciplina[]",
                    "name": "retorno_disciplina_aluno",
                    "type": "tuple[]"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "retornarUltimoId",
            "outputs": [
                {
                    "internalType": "uint256",
                    "name": "",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        }
    ];

    window.projetoAplicado = new web3js.eth.Contract(window.abi, window.address, {
        from: '0xC9939790463aa0E4D0EeaA61958d99D5C9A57c15',
        gasPrice: '3000000'
    });

})();
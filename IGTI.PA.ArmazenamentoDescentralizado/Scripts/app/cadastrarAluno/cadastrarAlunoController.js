﻿

(function () {

    'use strict';

    angular
        .module('app')
        .controller('cadastroAlunoController', ['$scope', '$http', 'toastr', '$localStorage', 'parseWithDate', 'serviceUpload'
            , function ($scope, $http, toastr, $localStorage, parseWithDate, serviceUpload) {

                var vm = this;
                vm.modelAluno = {
                    nome: "",
                    email: "",
                    nacionalidade: "",
                    naturalidade: "",
                    documento: "",
                    sexo: "M",
                    idBlockchain: "0",
                    hash: ""
                };

                vm.atualizarIdBlockchain = function () {
                    window.projetoAplicado.methods.retornarUltimoId().call(function (error, result) {
                        vm.modelAluno.idBlockchain = parseInt(result) + 1;
                        console.log("idBlockchain: " + vm.modelAluno.idBlockchain);
                    });
                };

                vm.cadastrarAlunoBlockchain = function () {

                    window.projetoAplicado.methods.registrarAluno([0, vm.modelAluno.nome, vm.modelAluno.nacionalidade, vm.modelAluno.sexo,
                        vm.modelAluno.naturalidade, vm.modelAluno.documento])
                        .send({},
                            function (error, result) {

                                if (error != null && error.code == 4001) {
                                    swal({
                                        title: 'Transação cancelada',
                                        text: result,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: 'OK',
                                        showConfirmButton: true
                                    }).then(
                                        function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                        }).catch(swal.noop);
                                } else {

                                    swal({
                                        title: 'Código transação',
                                        text: result,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: 'OK',
                                        closeOnCancel: false
                                    }).then(
                                        function (isConfirm) {
                                            if (isConfirm) {
                                                vm.modelAluno.hash = result;
                                                vm.registrarAluno();                                                
                                            }
                                        }).catch(swal.noop);

                                }
                            }).on('confirmation', function (confirmationNumber, receipt) {
                                if (confirmationNumber > 0 && confirmationNumber < 5)
                                    toastr.success(confirmationNumber + " transação confirmada!");

                            });
                };

                vm.registrarAluno = function () {
                    $http.post(window.urlApi + "Aluno/CadastrarAluno", angular.toJson(vm.modelAluno))
                        .then(function (response) {
                            toastr.success('Usuário cadastrado com sucesso!');
                            vm.buscarAlunos();

                        }, function errorCallback(error) {

                            toastr.error(error);

                        });
                };

                vm.buscarAlunos = function () {
                    $http.get(window.urlApi + "Aluno/BuscarAlunos")
                        .then(function (response) {

                            vm.listaAlunos = response.data;                            

                        }, function errorCallback(error) {

                            toastr.error(error);

                        });
                };

                vm.usuarioLogado = $localStorage['usuarioNome'];
                vm.tipoUsuario = $localStorage['tipoUsuario'];
                vm.atualizarIdBlockchain();
                vm.buscarAlunos();

                setInterval(function () {
                    vm.atualizarIdBlockchain();
                }, 8000);
            }]);
})();
﻿(function () {
    'use strict';

    JSON.parseWithStringDatePtBr = function (json) {
        return JSON.parse(json,
            function (key, value) {
                if (typeof value === 'string') {
                    var isodate1 = moment(value).isValid();
                    if (isodate1) {
                        var b = isodate1[1].split(/[-,.]/);

                        return new moment(value).format('DD/MM/YYYY HH:mm')
                        //return new Date(+b[0]);
                        //return new moment(Date(+b[0])).format('DD/MM/YYYY HH:mm');
                        //return new moment(Date(+b[0])).format('DD/MM/YYYY HH:mm');
                    }

                    var isodate2 = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/.exec(value);
                    if (isodate2)
                        return new moment(Date(+isodate2[1], +isodate2[2] - 1, +isodate2[3], +isodate2[4], +isodate2[5], +isodate2[6]).format('DD/MM/YYYY HH:mm'))
                }
                return value;
            });
    }

    var DatefromString = function (str, ddmmyyyy) {
        var m = str.match(/(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/);
        if (m[2] == "/") {
            if (ddmmyyyy === false)
                return new Date(+m[4], +m[1] - 1, +m[3], m[5] ? +m[5] : 0, m[6] ? +m[6] : 0, m[7] ? +m[7] : 0, m[8] ? +m[8] * 100 : 0);
            return new Date(+m[4], +m[3] - 1, +m[1], m[5] ? +m[5] : 0, m[6] ? +m[6] : 0, m[7] ? +m[7] : 0, m[8] ? +m[8] * 100 : 0);
        }
        return new Date(+m[1], +m[3] - 1, +m[4], m[5] ? +m[5] : 0, m[6] ? +m[6] : 0, m[7] ? +m[7] : 0, m[8] ? +m[8] * 100 : 0);
    }

    var app = angular
        .module('app');


    app.service('serviceMask', function () {
        this.refreshMask = function ($scope, moment) {
            //DataPicker  
            $("#inicioPrevisto").mask("99/99/9999", { placeholder: moment.localeData()._longDateFormat.L });
            $("#terminoPrevisto").mask("99/99/9999", { placeholder: moment.localeData()._longDateFormat.L });
            $("#terminoReal").mask("99/99/9999", { placeholder: moment.localeData()._longDateFormat.L });

            $(".mask-data-ptBr").mask("99/99/9999", { placeholder: moment.localeData()._longDateFormat.L });
        };
    });

    app.service('parseWithDate', function () {
        this.parseWithDate = function (json, moment) {
            return JSON.parse(json,
                function (key, value) {
                    if (typeof value === 'string') {

                        if (key == "arquivo")
                            return value;

                        var isodate1 = /^\/Date\((d|-|.*)\)\/$/.exec(value);
                        if (isodate1) {

                            var b = isodate1[1].split(/[-,.+]/);
                            var now = new Date(+b[0]);
                            var utcDate = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

                            var local = 'America/Sao_Paulo';
                            if (moment().locale() == 'pt-br')
                                local = 'America/Sao_Paulo'
                            else if (moment().locale() == 'en')
                                local = 'America/New_York'
                            else if (moment().locale() == 'es')
                                local = 'Europe/Madrid'

                            //return new Date(moment(moment(utcDate).tz(local)._d));
                            return moment(now)._d;
                        }

                        var isodate2 = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/.exec(value);
                        if (isodate2) {
                            var now = new Date(+isodate2[1], +isodate2[2] - 1, +isodate2[3], +isodate2[4], +isodate2[5], +isodate2[6]);
                            var utcDate = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

                            var local = 'America/Sao_Paulo';
                            if (moment().locale() == 'pt-br')
                                local = 'America/Sao_Paulo';
                            else if (moment().locale() == 'en')
                                local = 'America/New_York';
                            else if (moment().locale() == 'es')
                                local = 'Europe/Madrid';

                            //return new Date(moment(moment(utcDate).tz(local)._d));
                            return moment(now)._d;
                        }

                        var isodate3 = /(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/.exec(value);

                        if (isodate3) {

                            if (/^(\FVS-|CKL-|FVM-)/gi.exec(value));
                            return value;


                            var local = 'America/Sao_Paulo';
                            var format;
                            if (moment().locale() == 'pt-br') {
                                local = 'America/Sao_Paulo';
                                format = "DD/MM/YYYY";
                            } else if (moment().locale() == 'en') {
                                local = 'America/New_York';
                                format = "MM/dd/YYYY";
                            } else if (moment().locale() == 'es') {
                                local = 'Europe/Madrid';
                                format = "DD/MM/YYYY";
                            }


                            var dateMoment = moment(isodate3['input'], format);
                            var now = new Date(dateMoment);
                            var utcDate = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

                            return moment(now)._d.toLocaleDateString();
                            //return new Date(moment(moment(utcDate).tz(local)._d)).toLocaleDateString();
                        }
                    }
                    return value;
                });
        };

    });


})();

﻿(function () {
    'use strict';

    angular.module('app').controller('authenticationController', ['$scope', '$http', '$cookies', '$q', 'toastr', '$localStorage',
        function ($scope, $http, $cookies, $q, toastr, $localStorage) {


            var vm = this;

            vm.status = "Login";
            vm.isBusy = false;
            vm.isAdmin = false;
            vm.password = "";
            vm.email = "";
            
            vm.modalLogin = function () {
                $("#modalLoginForm").modal('show');
            };

            var token = $localStorage["access_token"];

            if (token != "" && token != undefined && location.href == "http://localhost/IGTI.ArmazenamentoDescentralizado/" ||
                location.href == "http://localhost/IGTI.ArmazenamentoDescentralizado/Home") {
                vm.tipoUsuario = $localStorage['tipoUsuario'];

                if (vm.tipoUsuario)
                    location.href = window.urlBase + "/Professor";
                else
                    location.href = window.urlBase + "/Aluno";
            }


            vm.sair = function () {
                $localStorage['access_token'] = undefined;
                location.href = window.urlBase + "/Home";

            };

            vm.authenticate = function () {

                vm.isBusy = true;
                vm.status = "Aguarde...";


                var body = 'grant_type=password&username=' + vm.email + '&password=' + vm.password;

                $http.post(window.urlApi + 'Token', body, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).then(function (response) {


                    $localStorage['access_token'] = response.data.access_token;
                    $localStorage['usuarioNome'] = response.data.usuarioNome;
                    $localStorage['tipoUsuario'] = response.data.tipoUsuario;
                    $cookies.put('access_token', response.data.access_token, { path: '/' });

                    if (response.data.tipoUsuario == 1)
                        location.href = window.urlBase + "/Professor";
                    else
                        location.href = window.urlBase + "/Aluno";

                }, function errorCallback(response) {

                    vm.status = "Login";
                    vm.isBusy = false;

                    toastr.error('Credenciais inválidas...');
                });

            };

        }]);

})();
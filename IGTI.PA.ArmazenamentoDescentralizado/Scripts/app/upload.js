﻿(function () {

    'use strict';

    var app = angular.module('app');
    app.service('serviceUpload', function () {

        this.Upload = function (toastr, vm, $scope, $localStorage) {

            //$(".fileupload").attr("accept", ".avi", ".mpg4", ".mp4", ".mov");
            var $fileInput = $('.fileupload');
            $fileInput.fileupload({
                dataType: 'json',
                url: window.urlApi + "Professor/Upload",
                autoUpload: true,
                headers: { 'Authorization': "Bearer " + $localStorage["access_token"] },
                add: function (e, data) {

                    var extension = "";

                    if ($scope.$parent.descricao == undefined || $scope.$parent.nome == undefined
                        || $scope.$parent.descricao == "" || $scope.$parent.nome == "") {
                        swal({
                            title: "Aviso",
                            text: "Preencha nome e descrição",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok"
                            //closeOnConfirm: true,
                            //closeOnCancel: false
                        }).then(
                            function (isConfirm) {
                                if (isConfirm) {


                                }
                            }).catch(swal.noop);
                        return;
                    }

                    for (var i = 0; i < data.files.length; i++) {
                        extension = data.files[i].name.split('.').pop().toLowerCase();

                        if (window.EXTENSIONSMOVIE.indexOf(extension) == -1) {
                            swal({
                                title: "Aviso",
                                text: "Extensão de arquivo inválida são permitido apenas: " + window.EXTENSIONSMOVIE,
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok"
                                //closeOnConfirm: true,
                                //closeOnCancel: false
                            }).then(
                                function (isConfirm) {
                                    if (isConfirm) {


                                    }
                                }).catch(swal.noop);


                        }
                    }

                    data.submit();
                },
                done: function (e, data) {

                    setTimeout(function () {
                        $('.progress').delay(100).fadeOut(300);
                    }, 1000);

                    toastr.success("Arquivo enviado com sucesso!");

                },
                fail: function (e, data) {

                    toastr.success("Tente novamente", "Erro");
                }

            }).on('fileuploadprogressall', function (e, data) {
                $('.progress').show();
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar').css('width', progress + '%');
                $('.progressoAtual').html(progress + '%');


            }).bind('fileuploaddone', function () {
                var activeUploads = $('.fileupload').fileupload('active');

            });
        };


    });

    var humanFileSize = function (size) {
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return (size / Math.pow(1024, i)).toFixed(2) * 1;
    };
})();
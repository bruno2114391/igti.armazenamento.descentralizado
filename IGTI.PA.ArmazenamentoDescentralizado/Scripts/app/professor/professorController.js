﻿

(function () {

    'use strict';

    angular
        .module('app')
        .controller('professorController', ['$scope', '$http', 'toastr', '$localStorage', 'parseWithDate', 'serviceUpload'
            , function ($scope, $http, toastr, $localStorage, parseWithDate, serviceUpload) {
                              
                var vm = this;
                $scope.fechamento = false;
                $scope.mensagem = "";
                $scope.nome = "";
                $scope.descricao = "";
                serviceUpload.Upload(toastr, vm, $scope, $localStorage);                
                
                var token = $localStorage["access_token"];

                vm.usuarioLogado = $localStorage['usuarioNome'];
                vm.tipoUsuario = $localStorage['tipoUsuario'];


                if (token == "" || token == undefined) {
                    $localStorage['access_token'] = undefined;
                    $localStorage['usuarioNome'] = undefined;
                    $localStorage['tipoUsuario'] = undefined;
                    location.href = window.urlBase + "/Home";
                }
                else if (vm.tipoUsuario == false)
                    location.href = window.urlBase + "/Aluno";


            }]);
})();
﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace IGTI.PA.ArmazenamentoDescentralizado.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            TesteAsync();
            return View();
        }

        public async System.Threading.Tasks.Task TesteAsync()
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "http://localhost:9980/renter/files?cached=false"))
                {
                    request.Headers.TryAddWithoutValidation("User-Agent", "Sia-Agent");

                    var response = await httpClient.SendAsync(request);
                    string rep = await response.Content.ReadAsStringAsync();
                }
            }
        }
    }
}

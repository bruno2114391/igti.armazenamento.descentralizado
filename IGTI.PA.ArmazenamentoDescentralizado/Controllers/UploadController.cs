﻿using IGTI.PA.ArmazenamentoDescentralizado.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace IGTI.PA.ArmazenamentoDescentralizado.Controllers
{
    public class UploadController : Controller
    {
        [System.Web.Http.HttpPost]
        public ContentResult UploadFiles()
        {
            dynamic mensagem = new ExpandoObject();
            string fileName = "";
            string extension = "";

            UploadFilesResult retorno = new UploadFilesResult();

            try
            {
                foreach (string file in Request.Files)
                {
                    int maxSizeUpload = 500000000; // 500Mb;

                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;

                    if (hpf.ContentLength > maxSizeUpload * 1024)
                        return
                            Content("{\"error\":\"Tamanho do anexo excede o limite permitido!\", \"maxSizeUpload\":\"" + maxSizeUpload + "\"}", "application/json");

                    string[] ext = new string[] { "txt", "avi", "mpg4", "mp4", "mov", "jpg", "png", "jpeg" };

                    extension = Extension(hpf.FileName);

                    if (!ext.Any(s => extension.Contains(s)))
                        return Content("{\"error\":\"extension\",\"extension\":\"" + String.Join(",", ext).ToUpper() + "\"}", "application/json");


                    using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
                    {

                        mensagem.Descricao = Request["Descricao"];
                        mensagem.Nome = retiraAcentos(hpf.FileName);
                        mensagem.Arquivo = binaryReader.ReadBytes(Request.Files[0].ContentLength);
                        mensagem.Extensao = extension;

                    }

                    #region Enviando dados para o REST


                    var client = new RestClient("http://localhost/IGTI.PA.ArmazenamentoDescentralizado.Api/api/");

                    var request = new RestRequest();


                    request = new RestRequest("ProjetoAplicado/Upload", Method.POST)
                    {
                        RequestFormat = DataFormat.Json
                    };


                    var token = "";
                    var httpCookie = Request.Cookies["access_token"];
                    if (httpCookie == null)
                        token = Request["Token"].Replace("\"", "").ToString();
                    else
                        token = httpCookie.Value;

                    request.AddHeader("Authorization", "Bearer " + token);

                    request.AddBody(JsonConvert.SerializeObject(mensagem, Formatting.None,
                        new JsonSerializerSettings { Culture = new CultureInfo("pt-BR") }));

                    var response = client.Execute(request);

                    if (response.StatusCode.Equals(HttpStatusCode.OK) && response.Content != null)
                        retorno = JsonConvert.DeserializeObject<UploadFilesResult>(response.Content);

                    else
                    {
                        return new ContentResult
                        {
                            Content = response.Content,
                            ContentType = "application/json"
                        };
                        
                    }

                    #endregion
                }

            }
            catch (Exception e)
            {
                ModelState.AddModelError("error", e.Message);
            }

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var resultData = retorno;
            var result = new ContentResult
            {
                Content = serializer.Serialize(resultData),
                ContentType = "application/json"
            };
            return result;

            // return Content(new JavaScriptSerializer().Serialize(retorno), "application/json");

        }

        public string Extension(string key)
        {
            return Path.GetExtension(key).Replace(".", "").ToLower();
        }

        public string retiraAcentos(string texto)
        {
            string ComAcentos = "!@#$%¨&*()-?:{}][ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç ";
            string SemAcentos = "_________________AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc_";

            for (int i = 0; i < ComAcentos.Length; i++)
                texto = texto.Replace(ComAcentos[i].ToString(), SemAcentos[i].ToString()).Trim();

            return texto;
        }

    }
}
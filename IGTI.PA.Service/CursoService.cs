﻿using IGTI.PA.Infrastructure;
using IGTI.PA.Models;
using System;
using Newtonsoft.Json.Linq;
using IGTI.PA.Interface;
using IGTI.PA.Core;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IGTI.PA.Service
{
    public class CursoService
    {
        private ProjetoAplicadoDbContext db = new ProjetoAplicadoDbContext();

        public dynamic Upload(Anexo anexo, IValidationDictionary validation)
        {
            try
            {
                db.Curso.Add(new Models.Entity.Curso()
                {
                    Ativo = true,
                    Caminho = anexo.Arquivo,
                    Descricao = anexo.Descricao,
                    Nome = anexo.Nome,
                    DataCad = DateTime.Now,
                    Extensao = anexo.Extensao
                });

                db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "Upload é inválido");
            }

            return true;

        }

        public async Task<dynamic> DeleteAsync(int idFile)
        {
            try
            {
                var file = db.Curso.Where(c => c.Id == idFile).FirstOrDefault();
                var result = await new SIA().DeleteFile(file.Nome);

                if (result)
                {
                    file.DataExclusao = DateTime.Now;
                    file.Ativo = false;
                    return db.SaveChanges() > 0 ? true : false;
                }
                return false;

            }
            catch (Exception)
            {
                throw new Exception("Upload é inválido");
            }
        }

        public async Task<dynamic> DownloadAsync(int idFile)
        {
            try
            {
                var file = db.Curso.Where(c => c.Id == idFile).FirstOrDefault();
                return new SIA().DownloadStreamAsync(file.Nome + "." + file.Extensao);

            }
            catch (Exception)
            {
                throw new Exception("Upload é inválido");
            }
        }

        public dynamic Get()
        {
            try
            {
                return db.Curso.Where(c => c.Sincronizado == true).ToList().Select(c => new
                {
                    c.Id,
                    c.Nome,
                    c.Caminho,
                    c.Descricao,
                    c.Extensao,
                    Src = @"http://localhost:9980/renter/stream/" + new Utilities().RemoveAcents(c.Descricao.Replace(" ", "")) + ".mp4"
                });

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}

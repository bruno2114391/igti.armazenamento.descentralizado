﻿using IGTI.PA.Infrastructure;
using IGTI.PA.Models;
using System;
using Newtonsoft.Json.Linq;
using IGTI.PA.Interface;
using IGTI.PA.Core;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IGTI.PA.Models.Entity;

namespace IGTI.PA.Service
{
    public class AlunoService
    {
        private ProjetoAplicadoDbContext db = new ProjetoAplicadoDbContext();

        public dynamic CadastrarAluno(JObject jAluno)
        {
            Usuario aluno;
            try
            {
                aluno = jAluno.ToObject<Usuario>();

                db.Usuario.Add(
                  new Usuario()
                  {
                      Nome = aluno.Nome,
                      Ativo = true,
                      Email = aluno.Email,
                      Senha = Utilities.HashHmac($"{aluno.Email}!@#$%QWE", "1"),
                      Professor = false,
                      Nacionalidade = aluno.Nacionalidade,
                      Naturalidade = aluno.Naturalidade,
                      Documento = aluno.Documento,
                      Sexo = aluno.Sexo,
                      DataCad = DateTime.Now,
                      IdBlockchain = aluno.IdBlockchain,
                      Hash = aluno.Hash
                  });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Não foi possível cadastrar aluno!");
            }

        }

        public dynamic CadastrarDisciplinaAluno(JObject jDisciplina)
        {
            DisciplinaAluno disciplina;
            try
            {
                disciplina = jDisciplina.ToObject<DisciplinaAluno>();

                db.DisciplinaAluno.Add(
                  new DisciplinaAluno()
                  {
                      Disciplina = disciplina.Disciplina,
                      Nota = disciplina.Nota,
                      Hash = disciplina.Hash,
                      DataCad = DateTime.UtcNow,
                      UsuarioId = db.Usuario.FirstOrDefault(u => u.IdBlockchain == disciplina.idBlockchain).Id
                  });
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Não foi possível cadastrar disciplina!");
            }

        }

        public dynamic BuscarAlunos()
        {
            try
            {
                return db.Usuario.Where(u => u.Professor == false).ToList();
            }

            catch (Exception)
            {
                throw new Exception("Não foi possível buscar usuários!");
            }
        }

        public dynamic BuscarDisciplinas()
        {
            try
            {
                return (from d in db.DisciplinaAluno
                        join u in db.Usuario on d.UsuarioId equals u.Id
                        select new
                        {
                            d.Id,
                            d.Disciplina,
                            d.Nota,
                            d.Hash,
                            u.Nome
                        }).ToList();
            }

            catch (Exception)
            {
                throw new Exception("Não foi possível buscar usuários!");
            }
        }

    }
}

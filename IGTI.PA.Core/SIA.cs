﻿using IGTI.PA.Infrastructure;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IGTI.PA.Core
{
    public class SIA
    {
        private ProjetoAplicadoDbContext db = new ProjetoAplicadoDbContext();


        //Upload 
        public async Task<bool> UploadStreamAsync(string path, string fileName)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("POST"),
                        string.Format(@"http://localhost:9980/renter/uploadstream/{0}?datapieces=10&paritypieces=20", fileName)))
                    {
                        SetHeader(request);
                        var streamContent = new StreamContent(File.Open(path, FileMode.Open));

                        request.Content = streamContent;

                        var response = await httpClient.SendAsync(request);
                        return response.IsSuccessStatusCode;
                    }
                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task DownloadStreamAsync(string fileName)
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"),
                    string.Format("http://localhost:9980/renter/stream/{0}", fileName)))
                {
                    request.Headers.TryAddWithoutValidation("User-Agent", "Sia-Agent");
                    request.Headers.TryAddWithoutValidation("Range", "bytes=0-1023");

                    var response = await httpClient.SendAsync(request);
                    string rep = await response.Content.ReadAsStringAsync();
                }
            }
        }

        //Status da Blockchain
        public async Task StatusBlockchainAsync()
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), "http://localhost:9980/consensus"))
                {
                    request.Headers.TryAddWithoutValidation("User-Agent", "Sia-Agent");

                    var response = await httpClient.SendAsync(request);
                    string rep = await response.Content.ReadAsStringAsync();
                }
            }
        }

        //Desbloquear Waller
        public async Task UnlockWallet()
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"),
                    "http://localhost:9980/wallet/unlock"))
                {

                    SetHeader(request);
                    request.Content = new StringContent("encryptionpassword=");
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                    var response = await httpClient.SendAsync(request);
                    string rep = await response.Content.ReadAsStringAsync();
                }
            }
        }

        //Sincronizar pasta local com a blockchain da sia
        public async Task SyncDataSiaAsync()
        {

            var file = db.Curso.Where(c => c.Sincronizado == false).FirstOrDefault();
            if (file != null)
            {
                var result = await UploadStreamAsync(file.Caminho, new Utilities().RemoveAcents(file.Descricao.Replace(" ", "")) + "." + file.Extensao);
                if (result)
                {
                    file.Sincronizado = true;
                    db.SaveChanges();
                }
            }


        }

        //Remover Arquivo 
        public async Task<bool> DeleteFile(string fileName)
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"),
                   string.Format("http://localhost:9980/renter/delete/{0}", fileName)))
                {
                    SetHeader(request);
                    var response = await httpClient.SendAsync(request);
                    return response.IsSuccessStatusCode;
                }
            }
        }

        //Autenticar
        private HttpRequestMessage SetHeader(HttpRequestMessage request)
        {
            var base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes(@"user="":603c3253603f88f7ccbc29b3f1494dc5"));

            request.Headers.TryAddWithoutValidation("User-Agent", "Sia-Agent");
            request.Headers.TryAddWithoutValidation("Content-Type", "application /x-www-form-urlencoded");
            request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}");

            return request;
        }
    }
}

﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace IGTI.PA.Core
{
    public class Utilities
    {

        public static string HashHmac(string secret, string senha)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(senha)))
            {
                var key = encoding.GetBytes(secret);
                var hash = hmac.ComputeHash(key);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }


        public static void SaveFileDisk(string path, byte[] bytes)
        {
            using (var file = new FileStream(path, FileMode.Create))
            {
                file.Write(bytes, 0, bytes.Length);
                file.Flush();
            }
        }

        public static void DeleteFileDisk(string filePathFull)
        {
            if (Directory.Exists(Path.GetDirectoryName(filePathFull)))
            {
                File.Delete(filePathFull);
            }
        }

        public string Extension(string key)
        {
            return Path.GetExtension(key).Replace(".", "").ToLower();
        }

        public string RemoveAcents(string texto)
        {
            string ComAcentos = "!@#$%¨&*()-?:{}][ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç ";
            string SemAcentos = "_________________AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc_";

            for (int i = 0; i < ComAcentos.Length; i++)
                texto = texto.Replace(ComAcentos[i].ToString(), SemAcentos[i].ToString()).Trim();

            return texto;
        }

    }
}

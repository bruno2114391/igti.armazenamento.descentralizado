﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisciplinaAluno : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DisciplinaAluno",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UsuarioId = c.Int(nullable: false),
                        Disciplina = c.String(),
                        Nota = c.String(),
                        Hash = c.String(),
                        UsuCadId = c.Int(),
                        UsuAltId = c.Int(),
                        DataCad = c.DateTime(),
                        DataAlt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuario", t => t.UsuarioId)
                .Index(t => t.UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DisciplinaAluno", "UsuarioId", "dbo.Usuario");
            DropIndex("dbo.DisciplinaAluno", new[] { "UsuarioId" });
            DropTable("dbo.DisciplinaAluno");
        }
    }
}

﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Aluno : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuario", "Nacionalidade", c => c.String());
            AddColumn("dbo.Usuario", "Naturalidade", c => c.String());
            AddColumn("dbo.Usuario", "Documento", c => c.String());
            AddColumn("dbo.Usuario", "Sexo", c => c.String());
            AddColumn("dbo.Usuario", "IdBlockchain", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuario", "IdBlockchain");
            DropColumn("dbo.Usuario", "Sexo");
            DropColumn("dbo.Usuario", "Documento");
            DropColumn("dbo.Usuario", "Naturalidade");
            DropColumn("dbo.Usuario", "Nacionalidade");
        }
    }
}

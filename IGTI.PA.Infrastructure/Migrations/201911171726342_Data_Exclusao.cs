﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Data_Exclusao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Curso", "DataExclusao", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Curso", "DataExclusao");
        }
    }
}

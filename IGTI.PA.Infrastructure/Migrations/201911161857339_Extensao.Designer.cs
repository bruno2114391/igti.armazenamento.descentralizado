﻿// <auto-generated />
namespace IGTI.PA.Infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0-preview3-19553-01")]
    public sealed partial class Extensao : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Extensao));
        
        string IMigrationMetadata.Id
        {
            get { return "201911161857339_Extensao"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Extensao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Curso", "Extensao", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Curso", "Extensao");
        }
    }
}

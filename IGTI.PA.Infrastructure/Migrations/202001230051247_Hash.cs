﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Hash : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuario", "Hash", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuario", "Hash");
        }
    }
}

﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Projeto_Aplicado : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Curso",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 50),
                        Ativo = c.Boolean(nullable: false),
                        Caminho = c.String(),
                        Descricao = c.String(),
                        UsuCadId = c.Int(),
                        UsuAltId = c.Int(),
                        DataCad = c.DateTime(),
                        DataAlt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 50),
                        Email = c.String(maxLength: 100),
                        Ativo = c.Boolean(nullable: false),
                        Senha = c.String(),
                        Professor = c.Boolean(nullable: false),
                        UsuCadId = c.Int(),
                        UsuAltId = c.Int(),
                        DataCad = c.DateTime(),
                        DataAlt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Usuario", new[] { "Email" });
            DropTable("dbo.Usuario");
            DropTable("dbo.Curso");
        }
    }
}

﻿using IGTI.PA.Models.Entity;
using System;
using System.Data.Entity.Migrations;
using System.Security.Cryptography;
using System.Text;

namespace IGTI.PA.Infrastructure.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ProjetoAplicadoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        public static string HashHmac(string secret, string senha)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(senha)))
            {
                var key = encoding.GetBytes(secret);
                var hash = hmac.ComputeHash(key);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }

        protected override void Seed(ProjetoAplicadoDbContext context)
        {
            context.Usuario.AddOrUpdate(usuario => usuario.Email,
               new Usuario()
               {
                   Nome = "Professor",
                   Ativo = true,
                   Email = "administrador@igti.com.br",
                   Senha = HashHmac("administrador@igti.com.br!@#$%QWE", "1"),
                   Professor = true
               });

           
        }
    }
}

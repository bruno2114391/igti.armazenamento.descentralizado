﻿namespace IGTI.PA.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sincronizar_Sia : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Curso", "Sincronizado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Curso", "Sincronizado");
        }
    }
}

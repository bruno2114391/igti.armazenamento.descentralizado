﻿using IGTI.PA.Models.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace IGTI.PA.Infrastructure
{
    public class ProjetoAplicadoDbContext : DbContext
    {
        public ProjetoAplicadoDbContext() : base("ProjetoAplicadoContext")
        {
            Database.SetInitializer<ProjetoAplicadoDbContext>(null);

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ProjetoAplicadoDbContext>());
            //definir o nome da tabela não pluralizada 
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //desabilitar uma exclusão em cascata em um join de ambas as tabelas envolvidas em um relacionamento de muitos a muitos
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            //desabiliar a exclusão em cascata para todas as relações necessárias.
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name));   

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Curso> Curso { get; set; }       
        public DbSet<DisciplinaAluno> DisciplinaAluno { get; set; }

    }
    
}

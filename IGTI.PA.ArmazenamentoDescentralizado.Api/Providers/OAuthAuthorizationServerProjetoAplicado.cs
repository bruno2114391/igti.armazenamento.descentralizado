﻿using IGTI.PA.Core;
using IGTI.PA.Infrastructure;
using IGTI.PA.Models.Entity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;


namespace IGTI.PA.ArmazenamentoDescentralizado.Api.Providers
{
    public class OAuthAuthorizationServerProjetoAplicado : OAuthAuthorizationServerProvider
    {
        private Usuario user;

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);

        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var dbContext = context.OwinContext.Get<ProjetoAplicadoDbContext>();
            try
            {
                var password = Utilities.HashHmac(context.UserName.ToLower() + "!@#$%QWE", context.Password.ToString());
                user = await dbContext.Usuario.FirstOrDefaultAsync(user =>
                user.Email == context.UserName.ToLower().ToString() &&
                user.Senha == password);

                if (user == null)
                    return;

            }
            catch (Exception e)
            {
                // Could not retrieve the user.
                context.SetError("server_error");
                context.Rejected();
                // Return here so that we don't process further. Not ideal but needed to be done here.
                return;
            }

            try
            {

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, user.Nome));
                identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));

                if (user.Professor)
                    identity.AddClaim(new Claim(ClaimTypes.Role, "IsProfessor"));

                AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties() { IsPersistent = true });
                var currentUtc = new SystemClock().UtcNow;
                ticket.Properties.IssuedUtc = currentUtc;
                ticket.Properties.ExpiresUtc = currentUtc.Add(TimeSpan.FromDays(365));
                string accessToken = context.Options.AccessTokenFormat.Protect(ticket);
                context.Validated(identity);

            }
            catch
            {
                // The ClaimsIdentity could not be created by the UserManager.
                context.SetError("server_error", "ERROSERVIDOR");
                context.Rejected();
            }


        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            if (user == null) return Task.FromResult<object>(null);
            context.AdditionalResponseParameters.Add("usuarioId", user.Id);
            context.AdditionalResponseParameters.Add("usuarioNome", user.Nome);
            context.AdditionalResponseParameters.Add("usuarioEmail", user.Email.ToLower());
            context.AdditionalResponseParameters.Add("tipoUsuario", user.Professor);


            foreach (var claim in context.Identity.Claims)
            {
                if (!claim.Type.Contains("role")) continue;
                var claimName = Char.ToLowerInvariant(claim.Value[0]) + claim.Value.Substring(1);
                context.AdditionalResponseParameters.Add(claimName, true);
            }

            return Task.FromResult<object>(null);
        }
    }
}
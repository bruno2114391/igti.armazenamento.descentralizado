using System;
using System.Reflection;

namespace IGTI.PA.ArmazenamentoDescentralizado.Api.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}
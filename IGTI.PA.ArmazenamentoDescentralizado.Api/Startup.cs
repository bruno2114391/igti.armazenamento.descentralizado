﻿using System;
using System.Threading.Tasks;
using Hangfire;
using IGTI.PA.ArmazenamentoDescentralizado.Api.Models;
using IGTI.PA.ArmazenamentoDescentralizado.Api.Providers;
using IGTI.PA.Core;
using IGTI.PA.Infrastructure;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(IGTI.PA.ArmazenamentoDescentralizado.Api.Startup))]

namespace IGTI.PA.ArmazenamentoDescentralizado.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var conn = @"Data Source=localhost;Initial Catalog=IGTI_PA; Integrated Security=False; User Id=sa;Password=NSS6BD;";
            GlobalConfiguration.Configuration.UseSqlServerStorage(conn);
            //app.UseHangfireDashboard();            
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter() }
            });
            Task.Run(() => new SIA().SyncDataSiaAsync());
            RecurringJob.AddOrUpdate(() => new SIA().SyncDataSiaAsync(), Cron.MinuteInterval(5));

            app.Use(async (context, next) =>
            {
                IOwinRequest req = context.Request;
                IOwinResponse res = context.Response;

                var origin = req.Headers.Get("Origin");
                if (!string.IsNullOrEmpty(origin))
                {
                    res.Headers.Set("Access-Control-Allow-Origin", origin);
                }
                if (req.Method == "OPTIONS")
                {
                    res.StatusCode = 200;
                    res.Headers.AppendCommaSeparatedValues("Access-Control-Allow-Credentials", "true");
                    res.Headers.AppendCommaSeparatedValues("Access-Control-Allow-Methods", "GET", "POST", "OPTIONS", "PUT", "DELETE");
                    res.Headers.AppendCommaSeparatedValues("Access-Control-Allow-Headers", "access-control-allow-origin", "accept", "access-control-allow-credentials", "access-control-allow-headers", "authorization", "access-control-allow-methods", "content-type");
                    return;
                }

                res.Headers.AppendCommaSeparatedValues("Access-Control-Allow-Credentials", "true");

                await next();
            });


            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);  // cors for owin token pipeline            
            ConfigureOAuth(app);

        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new ProjetoAplicadoDbContext());

            //http://bitoftech.net/2014/06/01/token-based-authentication-asp-net-web-api-2-owin-asp-net-identity/
            //http://www.devmedia.com.br/asp-net-identity-autenticacao-de-usuarios-com-claims/33131
            //http://bitoftech.net/2014/09/24/decouple-owin-authorization-server-resource-server-oauth-2-0-web-api/
            //http://bitoftech.net/2014/07/16/enable-oauth-refresh-tokens-angularjs-app-using-asp-net-web-api-2-owin/
            var oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/Token"),
                Provider = new OAuthAuthorizationServerProjetoAplicado(),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(5),
                AllowInsecureHttp = true,
                ApplicationCanDisplayErrors = true,

            };
            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
    }
}

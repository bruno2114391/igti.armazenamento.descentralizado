﻿using IGTI.PA.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IGTI.PA.ArmazenamentoDescentralizado.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Aluno")]
    public class AlunoController : ApiController
    {
        private readonly AlunoService alunoService = new AlunoService();


        [HttpPost]
        [ActionName("CadastrarAluno")]
        public dynamic CarrgarCurso(JObject jAluno)
        {
            try
            {
                var result = alunoService.CadastrarAluno(jAluno);
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }

        [HttpPost]
        [ActionName("CadastrarDisciplinaAluno")]
        public dynamic CadastrarDisciplinaAluno(JObject jAluno)
        {
            try
            {
                var result = alunoService.CadastrarDisciplinaAluno(jAluno);
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }


        [HttpGet]
        [ActionName("BuscarAlunos")]
        public dynamic BuscarAlunos()
        {
            try
            {
                var result = alunoService.BuscarAlunos();
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }

        [HttpGet]
        [ActionName("BuscarDisciplinas")]
        public dynamic BuscarDisciplinas()
        {
            try
            {
                var result = alunoService.BuscarDisciplinas();
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }

    }
}

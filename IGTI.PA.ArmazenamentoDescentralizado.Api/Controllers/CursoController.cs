﻿using IGTI.PA.Service;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IGTI.PA.ArmazenamentoDescentralizado.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Curso")]
    public class CursoController : ApiController
    {
        private readonly CursoService cursoService = new CursoService();


        [HttpGet]
        [ActionName("CarregarCurso")]
        public dynamic CarregarCurso()
        {
            try
            {
                var result = cursoService.Get();
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }
    }
}

﻿using IGTI.PA.ArmazenamentoDescentralizado.Models;
using IGTI.PA.Core;
using IGTI.PA.Models;
using IGTI.PA.Service;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IGTI.PA.ArmazenamentoDescentralizado.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Professor")]
    public class ProfessorController : ApiController
    {
        private readonly string fullPath;
        private readonly CursoService cursoService = new CursoService();

        public ProfessorController()
        {
            fullPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/Upload/");
        }


        [HttpPost]
        [ActionName("Upload")]
        [Authorize(Roles = "IsProfessor")]
        public async Task<bool> Upload()
        {
            try
            {

                var fileuploadPath = ConfigurationManager.AppSettings["FileUploadLocation"];

                var provider = new MultipartFormDataStreamProvider(fileuploadPath);
                var content = new StreamContent(HttpContext.Current.Request.GetBufferlessInputStream(true));
                foreach (var header in Request.Content.Headers)
                {
                    content.Headers.TryAddWithoutValidation(header.Key, header.Value);
                }

                await content.ReadAsMultipartAsync(provider);

                string uploadingFileName = provider.FileData.Select(x => x.LocalFileName).FirstOrDefault();
                string originalFileName = String.Concat(fileuploadPath, "\\" + (provider.Contents[2].Headers.ContentDisposition.FileName).Trim(new Char[] { '"' }));

                originalFileName = Path.ChangeExtension(originalFileName, ".mp4");

                if (File.Exists(originalFileName))
                {
                    File.Delete(originalFileName);
                }

                File.Move(uploadingFileName, originalFileName);


                var upload = cursoService.Upload(new Anexo()
                {
                    Arquivo = originalFileName,
                    Descricao = provider.FormData[0],
                    Extensao = "mp4",
                    Nome = provider.FormData[1]
                }, new ModelStateWrapper(this.ModelState));

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        [HttpDelete]
        [ActionName("Delete")]
        [Authorize(Roles = "IsProfessor")]
        public async Task<dynamic> DeleteAsync(int idFile)
        {
            try
            {
                var result = await cursoService.DeleteAsync(idFile);
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }

        [HttpGet]
        [ActionName("Download")]
        [Authorize(Roles = "IsProfessor")]
        public async Task<dynamic> Download(int idFile)
        {
            try
            {
                var result = await cursoService.DownloadAsync(idFile);
                return ModelState.IsValid ? result : Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ModelState);

            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.InnerException.InnerException.Message);
            }
        }



    }
}

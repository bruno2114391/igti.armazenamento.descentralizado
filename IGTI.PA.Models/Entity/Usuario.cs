﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGTI.PA.Models.Entity
{
    [Table("Usuario")]
    public class Usuario : BaseEntity
    {
        [StringLength(50)]
        public string Nome { get; set; }
        [StringLength(100)]
        [Index(IsUnique = true)]
        public string Email { get; set; }
        public bool Ativo { get; set; }
        public string Senha { get; set; }
        public bool Professor { get; set; }
        public string Nacionalidade { get; set; }
        public string Naturalidade { get; set; }
        public string Documento { get; set; }
        public string Sexo { get; set; }
        public int IdBlockchain { get; set; }
        public string Hash { get; set; }
    }
}

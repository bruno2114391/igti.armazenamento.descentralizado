﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace IGTI.PA.Models.Entity
{
    [Table("DisciplinaAluno")]
    public class DisciplinaAluno : BaseEntity
    {
        public int UsuarioId { get; set; }
        [ForeignKey("UsuarioId")]
        public virtual Usuario Usuario { get; set; }

        public string Disciplina { get; set; }
        public string Nota { get; set; }
        public string Hash { get; set; }

        [NotMapped]
        public int idBlockchain { get; set; }

    }
}

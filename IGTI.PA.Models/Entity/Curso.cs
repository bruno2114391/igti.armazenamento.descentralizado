﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGTI.PA.Models.Entity
{
    [Table("Curso")]
    public class Curso : BaseEntity
    {
        [StringLength(50)]
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public string Caminho { get; set; }
        public string Descricao { get; set; }
        public string Extensao { get; set; }
        public bool Sincronizado { get; set; }
        public DateTime? DataExclusao { get; set; }
    }
}

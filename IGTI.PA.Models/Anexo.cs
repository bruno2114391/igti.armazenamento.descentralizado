﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGTI.PA.Models
{
    public class Anexo
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Arquivo { get; set; }
        public string Extensao { get; set; }
    }
}

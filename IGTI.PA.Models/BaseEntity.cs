﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IGTI.PA.Models
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            DataCad = DateTime.UtcNow;
        }

        [Key]
        public int Id { get; set; }
        public int? UsuCadId { get; set; }
        public int? UsuAltId { get; set; }

        public DateTime? DataCad { get; set; }
        public DateTime? DataAlt { get; set; }
    }
}
